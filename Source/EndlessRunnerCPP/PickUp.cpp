// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUp.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "RunCharacterClass.h"

// Sets default values
APickUp::APickUp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMesh->SetupAttachment(RootComponent);
	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &APickUp::OnObjectBeginOverLap);
}

// Called when the game starts or when spawned
void APickUp::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APickUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APickUp::OnObjectBeginOverLap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ARunCharacterClass* runCharacter = Cast<ARunCharacterClass>(OtherActor)) // <- this is a filter
	{
		OnGet();
		Destroy();
	}
}

