// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RunCharacterClass.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDeath, ARunCharacterClass*, runChar);

UCLASS()
class ENDLESSRUNNERCPP_API ARunCharacterClass : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARunCharacterClass();

	bool isDead = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float TotalCoins = 0.0f;

	UPROPERTY(BlueprintAssignable)
		FDeath onDeath;

	UFUNCTION(BlueprintCallable)
		void AddCoin();

	UFUNCTION(BlueprintCallable)
		void Die();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
	class USpringArmComponent* SpringArm;

	UPROPERTY(VisibleAnywhere)
	class UCameraComponent* Camera;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
