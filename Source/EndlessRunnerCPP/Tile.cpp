// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "RunCharacterClass.h"
#include "TimerManager.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "Obstacle.h"
#include "PickUp.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	SetRootComponent(SceneComponent);

	AttachPoint = CreateDefaultSubobject<UArrowComponent>("AttachPoint");
	AttachPoint->SetupAttachment(RootComponent);

	BoxCollision = CreateDefaultSubobject<UBoxComponent>("BoxCollision");
	BoxCollision->SetupAttachment(RootComponent);

	BoxCollision->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnBeginOverlap);
	
	ObstacleSpawn = CreateDefaultSubobject<UBoxComponent>("ObstacleSpawn");
	ObstacleSpawn->SetupAttachment(RootComponent);

	PickUpSpawn = CreateDefaultSubobject<UBoxComponent>("PickUpSpawn");
	PickUpSpawn->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();

	SpawnObstacles();
	SpawnPickUps();
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATile::OnBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ARunCharacterClass* runCharacter = Cast<ARunCharacterClass>(OtherActor))
	{
		OnTarget.Broadcast(this);
	}
}

void ATile::SpawnObstacles()
{
	FVector RandomPoint = UKismetMathLibrary::RandomPointInBoundingBox(ObstacleSpawn->GetRelativeLocation(), ObstacleSpawn->GetScaledBoxExtent());

	FActorSpawnParameters SpawnParameters;

	AObstacle* SpawnedObstacle = GetWorld()->SpawnActor<AObstacle>(obstacleTypes[0], RandomPoint, FRotator(0), SpawnParameters);

	SpawnedObstacle->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
}

void ATile::SpawnPickUps()
{
	FVector RandomPointPickUp = UKismetMathLibrary::RandomPointInBoundingBox(PickUpSpawn->GetRelativeLocation(), PickUpSpawn->GetScaledBoxExtent());

	FActorSpawnParameters SpawnParametersPickUp;

	APickUp* SpawnedPickups = GetWorld()->SpawnActor<APickUp>(pickUpTypes[0], RandomPointPickUp, FRotator(90, 0, 0), SpawnParametersPickUp);

	SpawnedPickups->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
}

FVector ATile::GetAttachTransform()
{
	return AttachPoint->GetComponentLocation();
}

