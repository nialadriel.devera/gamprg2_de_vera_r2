// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterController.h"
#include "Kismet/GameplayStatics.h"
#include "RunCharacterClass.h"

void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();

	runCharacter = GetPawn<ARunCharacterClass>();
	Possess(runCharacter);
	runCharacter = Cast<ARunCharacterClass>(GetPawn());
}

void ARunCharacterController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!runCharacter->isDead)
	{
		runCharacter->AddMovementInput(runCharacter->GetActorForwardVector(), 1);
	}
}

void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);
}

void ARunCharacterController::MoveRight(float scale)
{
	runCharacter->AddMovementInput(runCharacter->GetActorRightVector(), scale);
}
