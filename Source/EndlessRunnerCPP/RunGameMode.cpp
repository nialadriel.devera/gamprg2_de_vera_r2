// Fill out your copyright notice in the Description page of Project Settings.


#include "RunGameMode.h"
#include "Kismet/GameplayStatics.h"
#include "Tile.h"
#include "Engine/World.h"	
#include "TimerManager.h"
#include "Obstacle.h"

void ARunGameMode::BeginPlay()
{
	Super::BeginPlay();

	for (int i = 0; i < 10; i++)
	{
		SpawnTile();
	}
}

void ARunGameMode::SpawnTile()
{
	FActorSpawnParameters SpawnParameters;

	ATile* SpawnedTile = GetWorld()->SpawnActor<ATile>(tile, Location, FRotator(0), SpawnParameters);
	Location = SpawnedTile->GetAttachTransform() + FVector(1000,0,0);

	SpawnedTile->OnTarget.AddDynamic(this, &ARunGameMode::OnExit);
}

void ARunGameMode::OnExit(ATile* tiles)
{
	SpawnTile();
	GetWorldTimerManager().SetTimer(timerHandle, FTimerDelegate::CreateUObject(this, &ARunGameMode::DestroyTile, tiles), 1.0f, false);
}

void ARunGameMode::DestroyTile(ATile* tiles)
{
	TArray<AActor*> obstacles;
	tiles->GetAttachedActors(obstacles);

	for (int i = 0; i < obstacles.Num(); ++i)
	{
		obstacles[i]->Destroy();
	}

	tiles->Destroy();
}

