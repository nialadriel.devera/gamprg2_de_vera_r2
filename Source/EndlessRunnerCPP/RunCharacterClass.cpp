// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterClass.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "RunCharacterController.h"
#include "Engine/Engine.h"


// Sets default values
ARunCharacterClass::ARunCharacterClass()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>("Spring Arm");
	SpringArm->SetupAttachment(RootComponent);

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	Camera->SetupAttachment(SpringArm);
	Camera->SetRelativeLocation(FVector(-400.0f, 0, 100));
}

// Called when the game starts or when spawned
void ARunCharacterClass::BeginPlay()
{
	Super::BeginPlay();
}

void ARunCharacterClass::AddCoin()
{
	TotalCoins++;
}

void ARunCharacterClass::Die()
{
	if (isDead == false)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Dead"));
		ARunCharacterController* controller = Cast<ARunCharacterController>(GetController());
		controller->DisableInput(controller);

		GetMesh()->SetVisibility(false);

		onDeath.Broadcast(this);
		isDead = true;
	}
	
}

// Called every frame
void ARunCharacterClass::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


