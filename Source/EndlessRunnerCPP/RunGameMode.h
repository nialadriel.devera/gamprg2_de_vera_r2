// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ENDLESSRUNNERCPP_API ARunGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	FVector Location = FVector(0);

protected:
	virtual void BeginPlay() override;

	FTimerHandle timerHandle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class ATile> tile;

	UFUNCTION()
		void SpawnTile();

	UFUNCTION()
		void OnExit(ATile* tiles);

	UFUNCTION()
		void DestroyTile(ATile* tiles);
	
private:
};
